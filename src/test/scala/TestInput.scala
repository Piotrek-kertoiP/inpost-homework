import org.apache.spark.sql.{Dataset, Encoder, SparkSession}
import pjoter.datasets.raw.{Delivery, MarketingConsent}

object TestInput {
  def load[T](batchNumber: Int, fileName: String)(implicit spark: SparkSession, encoder: Encoder[T]): Dataset[T] = {
    spark.read
      .schema(encoder.schema)
      .json(this.getClass.getResource(s"/batch$batchNumber/$fileName").getPath)
      .as[T]
  }

  def loadConsents(
    batchNumber: Int
  )(implicit spark: SparkSession, encoder: Encoder[MarketingConsent]): Dataset[MarketingConsent] =
    load[MarketingConsent](batchNumber, "consents.json")

  def loadPickups(
    batchNumber: Int
  )(implicit spark: SparkSession, encoder: Encoder[Delivery]): Dataset[Delivery] =
    load[Delivery](batchNumber, "pickups.json")
}
