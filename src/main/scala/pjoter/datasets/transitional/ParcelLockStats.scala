package pjoter.datasets.transitional

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{Dataset, SparkSession}
import pjoter.datasets.raw.Delivery

import java.sql.Timestamp

final case class ParcelLockStats(
  user_id: String,
  parcel_lock_id: String,
  use_count: Int,
  use_count_rank: BigInt,
  first_use_timestamp: Timestamp,
  first_use_rank: BigInt
)

object ParcelLockStats {
  def get(deliveries: Dataset[Delivery])(implicit spark: SparkSession): Dataset[ParcelLockStats] = {
    import spark.implicits._
    deliveries
      .withColumn(
        "use_count",
        count("delivery_timestamp").over(Window.partitionBy("user_id", "parcel_lock_id"))
      )
      .withColumn(
        "first_use_timestamp",
        min("delivery_timestamp").over(Window.partitionBy("user_id", "parcel_lock_id"))
      )
      .withColumn(
        "use_count_rank",
        dense_rank() over Window.partitionBy("user_id").orderBy(desc("use_count"))
      )
      .withColumn(
        "first_use_rank",
        dense_rank() over Window.partitionBy("user_id").orderBy(asc("first_use_timestamp"))
      )
      .select(
        col("user_id"),
        col("parcel_lock_id"),
        col("use_count") cast IntegerType,
        col("first_use_timestamp"),
        col("use_count_rank"),
        col("first_use_rank")
      )
      .as[ParcelLockStats]
  }

}
