
ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(name := "inpost-homework")

val sparkVsn = "3.2.2"
val scalaTestVsn = "3.0.9"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core"      % sparkVsn,
  "org.apache.spark" %% "spark-sql"       % sparkVsn,
  "org.apache.spark" %% "spark-hive"      % sparkVsn,
  "org.apache.spark" %% "spark-streaming" % sparkVsn,
  "org.apache.spark" %% "spark-core"      % sparkVsn,
  "org.apache.spark" %% "spark-sql"       % sparkVsn,
  "org.apache.spark" %% "spark-hive"      % sparkVsn,
  "org.apache.spark" %% "spark-streaming" % sparkVsn,
  "org.scalatest"    %% "scalatest"       % scalaTestVsn % Test

)
