import org.apache.spark.sql.SparkSession

import java.io.File
import java.util.UUID
import scala.reflect.io.Directory

trait SparkSpec {
  System.setProperty("derby.system.home", new File("target/derby").getAbsolutePath)
  //UUID at the end of path will allow multiple tests to run simultaneously
  lazy val sparkWarehousePath = new File(s"target/spark/${UUID.randomUUID()}").getAbsolutePath

  implicit lazy val spark: SparkSession =
    SparkSession
      .builder()
      .master("local")
      .config("hive.exec.dynamic.partition", "true")
      .config("hive.exec.dynamic.partition.mode", "nonstrict")
      .config("spark.sql.shuffle.partitions", 2)
      .config("spark.default.parallelism", 1)
      .config("spark.task.maxFailures", 1)
      .config("spark.sql.warehouse.dir", sparkWarehousePath)
      .config("spark.ui.enabled", "false")
      .config("spark.sql.legacy.timeParserPolicy", "LEGACY")
      .appName(this.getClass.getCanonicalName)
      .getOrCreate()

  def cleanUpWarehouse() = {
    val directory = new Directory(new File(sparkWarehousePath))
    directory.deleteRecursively()
  }
}
