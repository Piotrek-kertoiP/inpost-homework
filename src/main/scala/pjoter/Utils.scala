package pjoter

import org.apache.spark.sql.catalyst.TableIdentifier
import org.apache.spark.sql.catalyst.analysis.NoSuchTableException
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}

import java.net.URI

object Utils {
  //note: for simplicity let's assume we don't use partitioned tables as it's only for demo purposes

  def locationOf(db: String, table: String)(implicit spark: SparkSession): URI = {
    val catalog = spark.sessionState.catalog
    try {
      val id = TableIdentifier(table, Some(db))
      catalog.getTableMetadata(id).location
    } catch {
      case _: NoSuchTableException =>
        val dbLocation = catalog.getDatabaseMetadata(db).locationUri
        URI.create(s"$dbLocation/$table")
    }
  }

  def overwriteTable[T](ds: Dataset[T], db: String, table: String)(implicit spark: SparkSession): Unit = {
    val tableLocation = locationOf(db, table)
    ds.coalesce(1)
      .write
      .option("path", tableLocation.toString)
      .mode(SaveMode.Overwrite)
      .saveAsTable(table)
  }

  def appendTable[T](ds: Dataset[T], db: String, table: String)(implicit spark: SparkSession): Unit = {
    val fullTableName = s"$db.$table"
    if (!spark.catalog.tableExists(fullTableName)) {
      overwriteTable(ds, db, table)
    } else {
      val tableLocation = locationOf(db, table)
      ds.coalesce(1)
        .write
        .option("path", tableLocation.toString)
        .mode(SaveMode.Append)
        .saveAsTable(fullTableName)
    }
  }

}
