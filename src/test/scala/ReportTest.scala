import TestInput._
import org.scalatest.{BeforeAndAfterAll, FlatSpec, GivenWhenThen, Matchers}
import pjoter.datasets.result.Report

import java.sql.Date
import java.util.TimeZone

class ReportTest extends FlatSpec with Matchers with GivenWhenThen with BeforeAndAfterAll with SparkSpec {
  import spark.implicits._

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }

  TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
  spark.conf.set("spark.sql.session.timeZone", "UTC")

  val consents1 = loadConsents(1)
  val pickups1 = loadPickups(1)
  val consents2 = loadConsents(2)
  val pickups2 = loadPickups(2)
  val consents3 = loadConsents(3)
  val pickups3 = loadPickups(3)

  /** User perspective story (chronologically for each user) *
    * User | Day | Action
    *  1   |  1  | Gave marketing consent
    * ---------------------------------------------------------
    *  2   |  1  | Used parcel lock 10
    *  2   |  2  | Used parcel lock 11
    *  2   |  2  | Used parcel lock 11
    *  2   |  2  | Gave marketing consent
    *  2   |  3  | Used parcel lock 12
    *  2   |  3  | Declined marketing consent
    * ---------------------------------------------------------
    *  3   |  2  | Used parcel lock 10
    * ---------------------------------------------------------
    *  4   |  3  | Used parcel lock 12
    *  4   |  3  | Gave marketing consent
    * ---------------------------------------------------------
    *  5   |  3  | Used parcel lock 13
    * ---------------------------------------------------------*/
  "report" should "generate properly after 1st day" in {
    val report = Report.get(consents1, pickups1)
    report.collect() should contain theSameElementsAs Seq[Report](
      // @formatter:off
      /**    user_id  first_parcel_lock_id  most_used_parcel_lock_ids second_most_used_parcel_lock_ids  first_delivery_date               first_delivery_month  users_with_eq_first_delivery_date consent_recently_present  consent_currently_present */
      Report("1",     None,                 None,                     None,                             None,                             None,                 None,                             false,                    true),
      Report("2",     Some("10"),           Some(Set("10")),          Some(Set()),                      Some(Date.valueOf("2023-01-01")), Some(1),              Some(0),                          false,                    false)
      // @formatter:on
    )
  }

  "report" should "generate properly after 2nd day" in {
    val report = Report.get(consents1 union consents2, pickups1 union pickups2)
    report.collect() should contain theSameElementsAs Seq[Report](
      // @formatter:off
      /**    user_id  first_parcel_lock_id  most_used_parcel_lock_ids second_most_used_parcel_lock_ids  first_delivery_date               first_delivery_month  users_with_eq_first_delivery_date consent_recently_present  consent_currently_present */
      Report("1",     None,                 None,                     None,                             None,                             None,                 None,                             false,                    true),
      Report("2",     Some("10"),           Some(Set("11")),          Some(Set("10")),                  Some(Date.valueOf("2023-01-01")), Some(1),              Some(0),                          true,                     true),
      Report("3",     Some("10"),           Some(Set("10")),          Some(Set()),                      Some(Date.valueOf("2023-01-02")), Some(1),              Some(0),                          false,                    false)
      // @formatter:on
    )
  }

  "report" should "generate properly after 3rd day" in {
    val report = Report.get(consents1 union consents2 union consents3, pickups1 union pickups2 union pickups3)
    report.collect() should contain theSameElementsAs Seq[Report](
      // @formatter:off
      /**    user_id  first_parcel_lock_id  most_used_parcel_lock_ids second_most_used_parcel_lock_ids  first_delivery_date               first_delivery_month  users_with_eq_first_delivery_date consent_recently_present  consent_currently_present */
      Report("1",     None,                 None,                     None,                             None,                             None,                 None,                             false,                    true), 
      Report("2",     Some("10"),           Some(Set("11")),          Some(Set("10", "12")),            Some(Date.valueOf("2023-01-01")), Some(1),              Some(0),                          true,                     false),
      Report("3",     Some("10"),           Some(Set("10")),          Some(Set()),                      Some(Date.valueOf("2023-01-02")), Some(1),              Some(0),                          false,                    false),
      Report("4",     Some("12"),           Some(Set("12")),          Some(Set()),                      Some(Date.valueOf("2023-01-03")), Some(1),              Some(1),                          false,                    true), 
      Report("5",     Some("13"),           Some(Set("13")),          Some(Set()),                      Some(Date.valueOf("2023-01-03")), Some(1),              Some(1),                          false,                    false)
      // @formatter:on
    )
  }
}
