package pjoter.datasets.transitional

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SparkSession}

import java.sql.Date

final case class ParcelLockStatsSummary(
  user_id: String,
  first_parcel_lock_id: String,
  most_used_parcel_lock_ids: Set[String],
  second_most_used_parcel_lock_ids: Set[String],
  first_delivery_date: Date,
  users_with_eq_first_delivery_date: BigInt
)

object ParcelLockStatsSummary {
  def get(parcelLockStats: Dataset[ParcelLockStats])(implicit spark: SparkSession): Dataset[ParcelLockStatsSummary] = {
    import spark.implicits._
    parcelLockStats
      .select(
        col("user_id"),
        when(col("use_count_rank") === 1, col("parcel_lock_id")).as("most_used_parcel_lock_id"),
        when(col("use_count_rank") === 2, col("parcel_lock_id")).as("second_most_used_parcel_lock_id"),
        when(col("first_use_rank") === 1, col("first_use_timestamp")).as("first_use_timestamp"),
        when(col("first_use_rank") === 1, col("parcel_lock_id")).as("first_parcel_lock_id")
      )
      .groupBy("user_id")
      .agg(
        collect_set(col("most_used_parcel_lock_id")) as "most_used_parcel_lock_ids",
        collect_set(col("second_most_used_parcel_lock_id")) as "second_most_used_parcel_lock_ids",
        first("first_parcel_lock_id", true) as "first_parcel_lock_id",
        first("first_use_timestamp", true) as "first_delivery_timestamp"
      )
      .select(
        "user_id",
        "first_parcel_lock_id",
        "most_used_parcel_lock_ids",
        "second_most_used_parcel_lock_ids",
        "first_delivery_timestamp"
      )
      .withColumn("first_delivery_date", to_date(col("first_delivery_timestamp")))
      .withColumn(
        "users_with_eq_first_delivery_date",
        count(col("user_id")).over(Window.partitionBy("first_delivery_date")) - lit(1)
      )
      .as[ParcelLockStatsSummary]
  }
}
