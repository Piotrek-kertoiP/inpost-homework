package pjoter.datasets.raw

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, SparkSession}
import pjoter.Utils.appendTable

import java.sql.Timestamp

final case class MarketingConsent(
  user_id: String,
  consent_granted: Boolean,
  consent_timestamp: Timestamp
)

object MarketingConsent {

  def fetch(now: Timestamp)(implicit spark: SparkSession): Dataset[MarketingConsent] = {
    import spark.implicits._
    spark
      .table("inpost.marketing_consents")
      .where(col("consent_timestamp") < now)
      .as[MarketingConsent]
  }

  def processBatch(newMarketingConsents: Dataset[MarketingConsent])(implicit spark: SparkSession): Unit = {
    appendTable(newMarketingConsents, "inpost", "marketing_consents")
  }
}
