package pjoter.datasets.raw

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, SparkSession}
import pjoter.Utils.appendTable

import java.sql.Timestamp

final case class Delivery(
  user_id: String,
  parcel_lock_id: String,
  delivery_timestamp: Timestamp
)

object Delivery {
  val tableName = "deliveries"

  def fetch(now: Timestamp)(implicit spark: SparkSession): Dataset[Delivery] = {
    import spark.implicits._
    spark
      .table(s"inpost.$tableName")
      .where(col("delivery_timestamp") < now)
      .as[Delivery]
  }

  def processBatch(deliveries: Dataset[Delivery])(implicit spark: SparkSession): Unit = {
    appendTable(deliveries, "inpost", tableName)
  }
}
