package pjoter.datasets.result

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SparkSession}
import pjoter.datasets.raw.{Delivery, MarketingConsent}
import pjoter.datasets.transitional._

import java.sql.Date

final case class Report(
  user_id: String,
  first_parcel_lock_id: Option[String],
  most_used_parcel_lock_ids: Option[Set[String]],
  second_most_used_parcel_lock_ids: Option[Set[String]],
  first_delivery_date: Option[Date],
  first_delivery_month: Option[Int],
  users_with_eq_first_delivery_date: Option[BigInt],
  consent_recently_present: Boolean,
  consent_currently_present: Boolean
)

object Report {
  def get(consents: Dataset[MarketingConsent], deliveries: Dataset[Delivery])(
    implicit spark: SparkSession
  ): Dataset[Report] = {
    import spark.implicits._

    //normally I'd use a 'select distinct user_id from table customers' as a base for joins, but here I'm using only 2 flows
    val allUsers = (consents.select("user_id") union deliveries.select("user_id")).distinct

    val parcelLockStats = ParcelLockStats.get(deliveries)
    val parcelLockStatsSummary = ParcelLockStatsSummary
      .get(parcelLockStats)
      .withColumn("first_delivery_month", month(col("first_delivery_date")))

    val recentConsents = RecentConsents.get(consents, deliveries)

    allUsers
      .join(parcelLockStatsSummary, Seq("user_id"), "left_outer")
      .join(recentConsents, Seq("user_id"), "left_outer")
      .select(
        allUsers("user_id"),
        parcelLockStatsSummary("first_parcel_lock_id"),
        parcelLockStatsSummary("most_used_parcel_lock_ids"),
        parcelLockStatsSummary("second_most_used_parcel_lock_ids"),
        parcelLockStatsSummary("first_delivery_date"),
        parcelLockStatsSummary("first_delivery_month"),
        parcelLockStatsSummary("users_with_eq_first_delivery_date"),
        coalesce(recentConsents("consent_recently_present"), lit(false)).as("consent_recently_present"),
        coalesce(recentConsents("consent_currently_present"), lit(false)).as("consent_currently_present")
      )
      .as[Report]
  }
}
