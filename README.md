# inpost homework

##Źródło danych codziennie dostarcza:

###źródło 1:
- id usera, 
- id paczkomatu, 
- timestamp (odbioru paczki)
###źródło 2: 
- id usera, 
- czy zgoda marketingowa (boolean), 
- timestamp (nadania/odebrania zgody)

###Zaproponuj tabelkę/tabelki w której przetrzymywać będziesz te dane i stworzyć funkcję która codziennie policzy:

- id usera
- id pierwszego paczkomatu z którego skorzystał
- najczęściej używany paczkomat
- drugi najczęściej używany paczkomat
- datę odbioru pierwszej paczki
- miesiąc odbioru pierwszej paczki
- liczbę użytkowników którzy tego samego dnia odebrali swoją pierwszą paczkę
- czy w dniu odebrania ostatniej paczki miał zgodę marketingową
- czy w dniu tworzenia raportu ma zgodę marketingową