package pjoter.datasets.transitional

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{coalesce, col, lit, max}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import pjoter.datasets.raw.{Delivery, MarketingConsent}

final case class RecentConsents(
  user_id: String,
  consent_recently_present: Boolean,
  consent_currently_present: Boolean
)

object RecentConsents {
  def getLatestDeliveries(deliveries: Dataset[Delivery])(implicit spark: SparkSession): Dataset[Delivery] = {
    import spark.implicits._
    deliveries
      .withColumn("last_delivery_timestamp", max("delivery_timestamp").over(Window.partitionBy("user_id")))
      .where(col("delivery_timestamp") === col("last_delivery_timestamp"))
      .select(
        "user_id",
        "parcel_lock_id",
        "delivery_timestamp"
      )
      .as[Delivery]
  }

  def getLatestConsents(marketingConsents: Dataset[MarketingConsent]) = {
    marketingConsents
      .withColumn(
        "last_consent_timestamp",
        max("consent_timestamp").over(Window.partitionBy("user_id"))
      )
      .where(col("consent_timestamp") === col("last_consent_timestamp"))
      .select("user_id", "consent_granted")
  }

  def get(
    consents: Dataset[MarketingConsent],
    deliveries: Dataset[Delivery]
  )(implicit spark: SparkSession): Dataset[RecentConsents] = {
    import spark.implicits._
    val lastDeliveries = getLatestDeliveries(deliveries)
    val currentConsents = getLatestConsents(consents)
      .withColumnRenamed("consent_granted", "consent_currently_present")

    val consentsBeforeLastDelivery = lastDeliveries
      .join(consents, Seq("user_id"))
      .where(consents("consent_timestamp") < lastDeliveries("delivery_timestamp"))
      .select("user_id", "consent_granted", "consent_timestamp")
      .as[MarketingConsent]

    val consentsWhenLastDelivered = getLatestConsents(consentsBeforeLastDelivery)
      .withColumnRenamed("consent_granted", "consent_recently_present")

    consentsWhenLastDelivered
      .join(currentConsents, Seq("user_id"), "full")
      .select(
        col("user_id"),
        consentsWhenLastDelivered("consent_recently_present"),
        currentConsents("consent_currently_present")
      )
      .as[RecentConsents]
  }
}
